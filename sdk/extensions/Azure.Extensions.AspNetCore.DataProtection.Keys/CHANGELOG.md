# Release History

## 1.0.1 (2020-08-06)

### Fixed

- Deadlock on .NET Framework (https://github.com/Azure/azure-sdk-for-net/pull/12605)

## 1.0.0 (2020-06-04)

- No changes. General availability release.

## 1.0.0-preview.2 (2020-05-05)

- Package renamed to Azure.Extensions.AspNetCore.DataProtection.Keys
- Default overload of ProtectKeysWithAzureKeyVault now takes a Uri to be consistent with other extension methods and KeyVault clients.  

## 1.0.0-preview.1 (2020)

- Initial preview of the Azure.AspNetCore.DataProtection.Keys library

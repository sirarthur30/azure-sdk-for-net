# Release History

## 12.0.0-preview.4 (Unreleased)


## 12.0.0-preview.3 (2020-08-13)
- This release contains bug fixes to improve quality.

## 12.0.0-preview.2 (2020-07-27)
- This release contains bug fixes to improve quality.

## 12.0.0-preview.1 (2020-07-03)
- This preview is the first release supporting Azure Storage Blobs Change Feed.
